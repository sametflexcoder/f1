/* ----------------------------------------*/
//App
/* ----------------------------------------*/

var app = angular.module("f1champions", ["chieffancypants.loadingBar"]);

app.run(function () {
    jcf.replaceAll(); // custom select element plugin
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('jsonInterceptor'); // handle ajax headers
});

app.config(function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = true; // custom loading bar plugin
});

app.constant('API', 'http://ergast.com/api/f1/');

/* ----------------------------------------*/
//Controllers
/* ----------------------------------------*/

app.controller('globalController', function ($http, API, $scope) {
    
    var vm = this;
    
    $scope.getRaceWinnersInYear = function (year){
        if (year == ""){
            return;
        }
        $scope.championID = "";
        
        $http({
            method: 'GET',
            url: API + year + "/results/1.json", //the number (1) is indicating finishing position of driver
        }).then(function successCallback(response) {
                $scope.races = response.data;
                vm.getChampionOfTheYear(year);
                $scope.selected_year = String($scope.selected_year)// fix auto selection for select item in html
                setTimeout(function(){// custom select element plugin
                    jcf.replaceAll();
                },0)
            },
            function errorCallback(response) {
                swal("", "There is an error to getting race data :(");
                console.log("error: ", response)
            });
    }
    
    vm.getChampionOfTheYear = function (year){
        $http({
            method: 'GET',
            url: API + year + "/driverStandings/1.json", //the number (1) is indicating finishing position of driver
        }).then(function successCallback(response) {
                $scope.champion = response.data
                $scope.championID = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0].Driver.driverId;
            },
            function errorCallback(response) {
                swal("", "There is an error to getting Champion of World data :(");
                console.log("error: ", response)
            });
    }
    
    $scope.next = function (){
        if($scope.selected_year < 2015 && $scope.selected_year != ""){
            $scope.selected_year++
            $scope.getRaceWinnersInYear($scope.selected_year);
        } else if($scope.selected_year == "" || $scope.selected_year == undefined){
            swal("", "Please select year");
        }
    }
    
    $scope.prev = function (){
        if($scope.selected_year > 2005 && $scope.selected_year != ""){
            $scope.selected_year--;
            $scope.getRaceWinnersInYear($scope.selected_year);
        } else if($scope.selected_year == "" || $scope.selected_year == undefined){
            swal("", "Please select a year");
        }
    }
    
    $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push(i);
        }
        return input;
    };
});

/* ----------------------------------------*/
//Services and Factories
/* ----------------------------------------*/

app.factory('jsonInterceptor', function (API, cfpLoadingBar, $q, $injector) { // handle loading bar and ajax headers
    return {
        request: function (config) {            
            cfpLoadingBar.start();
            return config;
        },
        response: function (response) {
            cfpLoadingBar.complete();
            return response;
        },
    }
})


